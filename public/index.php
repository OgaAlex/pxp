<?php

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

$ability_labels = ['god' => 'Бессмертие', 'fly' => 'Полет', 'idclip' => 'Прохождение сквозь стены', 'fireball' => 'Файрболлы'];

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
    print('Спасибо, результаты сохранены.');
  }
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
$errors = FALSE;

if (empty($_POST['fio'])) {
  print('Заполните имя.<br/>');
  $errors = TRUE;
}
else if (!preg_match('/^[а-яА-Я ]+$/u', $_POST['fio'])) {
  print('Недопустимые символы в имени.<br/>');
  $errors = TRUE;
}

if (empty($_POST['sex'])) {
  print('Заполните пол.<br/>');
  $errors = TRUE;
}
if (empty($_POST['con'])) {
  print('Заполните количество конечностей.<br/>');
  $errors = TRUE;
}

if (empty($_POST['year'])) {
    print('Заполните год.<br/>');
    $errors = TRUE;
}
else {
  $year = $_POST['year'];
  if (!(is_numeric($year) && intval($year) >= 1900 && intval($year) <= 2020)) {
    print('Укажите корректный год.<br/>');
    $errors = TRUE;
  }
}

$ability_data = array_keys($ability_labels);
if (empty($_POST['abilities'])) {
    print('Выберите способность.<br/>');
    $errors = TRUE;
}

$ability_insert = [];
foreach ($ability_data as $ability) {
  $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
}
$superpower = "";
foreach ($_POST['superpower'] as $keys=>$values) { $superpower .= $values;}



// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
// *************

if ($errors) {
  // При наличии ошибок завершаем работу скрипта.
  exit();
}

// Сохранение в базу данных.

$user = 'u20370';
$pass = '2394375';
$db = new PDO('mysql:host=localhost;dbname=u20370', $user, $pass,
  array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.
try {
  $stmt = $db->prepare("INSERT INTO application SET name = ?, email= ?, year = ?, sex=?, con=?, ability_god = ?, ability_fly = ?, ability_idclip = ?, ability_fireball = ?, Biography = ?");
  $stmt->execute([$_POST['fio'], $_POST['email'], intval($year), $_POST['sex'], $_POST['con'], $ability_insert['god'], $ability_insert['fly'], $ability_insert['idclip'], $ability_insert['fireball'], $_POST['Biography']]);
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

//  stmt - это "дескриптор состояния".
 
//  Именованные метки.
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));
 
//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);
$stmt->bindParam(':email', $email);
$firstname = "John";
$lastname = "Smith";
$email = "john@test.com";
$stmt->execute();
*/

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
header('Location: ?save=1');


